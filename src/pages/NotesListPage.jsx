import { useEffect, useState } from "react";
// import notes from "../assets/data";
import ListItem from "../components/ListItem";
import { FaNoteSticky } from "react-icons/fa6";
import AddButton from "../components/AddButton";
import SearchBar from "../components/SearchBar";
import SearchResultsList from "../components/SearchResultsList";

const NotesListPage = () => {
  const [notes, setNotes] = useState([]);

  useEffect(() => {
    const getNotes = async () => {
      const response = await fetch(`http://localhost:8000/notes`);
      const data = await response.json();
      setNotes(data);
    };

    getNotes();
  }, []);

  const [results, setResults] = useState("");
  let counter = results ? results : notes;

  return (
    <div className="notes">
      <div className="notes-header">
        <h2 className="notes-title">
          <FaNoteSticky /> Notes
        </h2>
        <p className="notes-count">{counter.length}</p>
      </div>
      <SearchBar setResults={setResults} />
      {results && <SearchResultsList results={results} />}
      {!results && (
        <div className="notes-list">
          {notes
            .sort((a, b) => (a.updated < b.updated ? 1 : -1))
            .map((note) => (
              <ListItem key={note.id} note={note} />
            ))}
        </div>
      )}
      <AddButton />
    </div>
  );
};

export default NotesListPage;
