import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Header from "./components/Header";
import NotesListPage from "./pages/NotesListPage";
import NotePage from "./pages/NotePage";
import NightModeButton from "./components/NightModeButton";
import { useState } from "react";

function App() {
  let [selectedMode, setMode] = useState(true);
  function handleToggle() {
    if (selectedMode === true) selectedMode = undefined;
    else if (selectedMode === undefined) selectedMode = true;
    setMode(selectedMode);
  }
  return (
    <Router>
      <div className={selectedMode ? "container dark" : "container"}>
        <div className="app">
          <Header />
          <Routes>
            <Route path="/" element={<NotesListPage />} />
            <Route path="/note/:id" element={<NotePage />} />
          </Routes>
        </div>
        <NightModeButton isDarkMode={selectedMode} onToggle={handleToggle} />
      </div>
    </Router>
  );
}

export default App;
