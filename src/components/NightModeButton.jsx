import React from "react";
import { FaMoon, FaSun } from "react-icons/fa6";

const NightModeButton = ({ onToggle, isDarkMode }) => {
  return (
    <button onClick={onToggle} className="night-mode-button">
      {isDarkMode && <FaSun />}
      {!isDarkMode && <FaMoon />}
    </button>
  );
};

export default NightModeButton;
