const DeleteModal = ({ onCancel, onDelete }) => {
  return (
    <div className="delete-modal-open">
      <p>Are you sure you want to delete this note?</p>
      <button onClick={onCancel} id="cancel-button">
        Cancel
      </button>
      <button onClick={onDelete} id="sure-button">
        Delete
      </button>
    </div>
  );
};

export default DeleteModal;
