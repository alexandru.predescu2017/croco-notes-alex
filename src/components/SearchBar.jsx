import React, { useState } from "react";
import { FaMagnifyingGlass } from "react-icons/fa6";

const SearchBar = ({ setResults }) => {
  const [input, setInput] = useState("");

  const fetchData = (value) => {
    fetch("http://localhost:8000/notes")
      .then((response) => response.json())
      .then((json) => {
        const results = json.filter((note) => {
          const lowerCaseValue = value.toLowerCase();
          const lowerCaseBody = note.body.toLowerCase();
          return lowerCaseBody.includes(lowerCaseValue);
        });
        setResults(results);
      });
  };

  const handleGhange = (value) => {
    setInput(value);
    fetchData(value);
  };

  return (
    <div className="search-bar">
      <FaMagnifyingGlass />
      <input
        className="search-area"
        placeholder="Search..."
        value={input}
        onChange={(e) => handleGhange(e.target.value)}
      />
    </div>
  );
};

export default SearchBar;
