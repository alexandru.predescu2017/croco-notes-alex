import React from "react";
import ListItem from "./ListItem";
const SearchResultsList = ({ results }) => {
  return (
    <div className="notes-list">
      {results
        .sort((a, b) => (a.updated < b.updated ? 1 : -1))
        .map((note) => (
          <ListItem key={note.id} note={note} />
        ))}
    </div>
  );
};

export default SearchResultsList;
