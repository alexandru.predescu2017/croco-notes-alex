const Header = () => {
  return (
    <div className="app-header">
      <h1>Croco Notes</h1>
    </div>
  );
};

export default Header;
