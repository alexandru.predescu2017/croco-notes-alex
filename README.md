# Croco Notes

Hey, crocos! Acesta este repository-ul pentru training-ul de React 2024. Mai jos gasiti instructiuni utile despre cum se poate rula proiectul.

#### React + Vite

Pasi necesari:

1. Clonati repository-ul folosind comanda `git clone` intr-un terminal de git bash.
2. Deschideti folder-ul clonat in VS Code.
3. Rulati intr-un terminal comanda `npm i` pentru a adauga modulele necesare proiectului.
4. Pentru a vizualiza proiectul in browser, rulati comanda `npm run dev`, iar apoi apasati <kbd>ctrl</kbd> + click pe URL-ul din terminal (http://localhost:3000)

#### JSON Server

Pasi necesari:

1. Instalare folosind intr-un terminal comanda: `npm install -g json-server@0.17.4`
2. Pornire json-server folosind in terminal comanda: `npm run server`

#### Task-uri

Mai jos aveti descrise task-urile pe care le aveti de realizat pentru a continua aplicatia.

1. Functionalitate de search in notite.
2. Implementare buton toggle light/dark mode.
3. Sortati notitele in ordine invers cronologica (cele mai recent actualizate sa apara primele).
4. **Optional**: Vorbiti cu un membru al diviziei back-end si realizati impreuna API-ul necesar operatiilor CRUD (Create, Read, Update, Delete), avand o baza de date relationala.
